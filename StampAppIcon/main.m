//
//  main.m
//  StampAppIcon
//
//  The MIT License (MIT)
//
// Copyright (c) 2013 Ivan Fuller 
//
//Permission is hereby granted, free of charge, to any person obtaining a copy
//of this software and associated documentation files (the "Software"), to deal
//in the Software without restriction, including without limitation the rights
//to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//copies of the Software, and to permit persons to whom the Software is
//furnished to do so, subject to the following conditions:
//
//The above copyright notice and this permission notice shall be included in
//all copies or substantial portions of the Software.
//
//THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//THE SOFTWARE.
//

#import <Foundation/Foundation.h>
#import <Quartz/Quartz.h>

CGSize dimensionsForImageFile( NSString* fileName )
{
    NSURL* imageFileURL = [NSURL fileURLWithPath:fileName];
    CGImageSourceRef imageSource = CGImageSourceCreateWithURL((__bridge CFURLRef)(imageFileURL), NULL);
    if (imageSource == NULL)
    {
        // Error loading image
        return CGSizeZero;
    }
    
    CGFloat width = 0.0f;
    CGFloat height = 0.0f;
    CFDictionaryRef cfimageProperties = CGImageSourceCopyPropertiesAtIndex(imageSource, 0, NULL);
    NSDictionary* imageProperties = (__bridge NSDictionary*)cfimageProperties;
    if (imageProperties != NULL)
    {
        width = [imageProperties[(NSString*)kCGImagePropertyPixelWidth] floatValue];
        height = [imageProperties[(NSString*)kCGImagePropertyPixelHeight] floatValue];       
    }
    CFRelease(cfimageProperties);

    return CGSizeMake(width, height);
}



int main(int argc, const char * argv[])
{
    
    @autoreleasepool
    {
        
        NSArray *args = [[NSProcessInfo processInfo] arguments];
        if ( [args count] == 5 )
        {
            NSString* quartzComposition = [[NSString stringWithUTF8String:argv[1]] stringByStandardizingPath];
            NSString* sourceImage = [[NSString stringWithUTF8String:argv[2]] stringByStandardizingPath];
            NSString* versionString = [NSString stringWithUTF8String:argv[3]];
            NSString* destImage = [[NSString stringWithUTF8String:argv[4]] stringByStandardizingPath];
            
            CGSize imageSize = dimensionsForImageFile( sourceImage );
            
            QCRenderer* renderer = nil;
            {
                CGColorSpaceRef colorSpace = CGColorSpaceCreateWithName ( kCGColorSpaceGenericRGB );
                QCComposition* composition = [QCComposition compositionWithFile:quartzComposition];
                if ( composition == nil )
                {
                    NSLog(@"Error: Failed to open Quartz Composition %@", quartzComposition);
                    exit ( 1 );
                }
                renderer = [[QCRenderer alloc] initOffScreenWithSize:imageSize colorSpace:colorSpace composition:composition];
                if ( renderer == nil )
                {
                    NSLog(@"Error: Failed to create renderer from Quartz Composition %@", quartzComposition);
                    exit ( 2 );
                }
                CGColorSpaceRelease(colorSpace);
            }
            
            NSBitmapImageRep* composedImage = nil;
            {
                [renderer setValue:sourceImage forInputKey:@"SourceImage"];
                [renderer setValue:versionString forInputKey:@"VersionString"];
                [renderer renderAtTime:0.0 arguments:nil];
                composedImage = [renderer createSnapshotImageOfType:@"NSBitmapImageRep"];
            }
            
            {
                NSData* renderedImageData = [composedImage representationUsingType:NSPNGFileType properties:nil];
                BOOL success = [renderedImageData writeToFile:destImage atomically:YES];
                if ( success == NO )
                {
					NSLog(@"Error: Failed to write rendered image %@", destImage);
                }
                
            }
            
        }
        else
        {
            NSLog(@"Usage: %@ quartzComposition sourceImage versionString destImage\n", [args[0] lastPathComponent]);
        }
        
        
    }
    return 0;
}

