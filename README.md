StampAppIcon
============

Command line utility that stamps a version number into an iOS app icon using Quartz Composer

This is a very rough work in progress. You'd be crazy to use it as-is without further refinement.

Usage
=====

StampAppIcon quartzComposition sourceImage versionString destImage

where:
* quartzComposition - a Quartz Composition that accepts two inputs: SourceImage and VersionString. A simple example is included in this repo.
* sourceImage - the image to stamp. (An app icon)
* versionString - the string to send to the composition.
* destImage - Your modified icon png will be written to this file.
